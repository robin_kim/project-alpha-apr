from django.urls import path
from accounts.views import accountlogin, accountlogout, accountsignup


urlpatterns = [
    path("login/", accountlogin, name="login"),
    path("logout/", accountlogout, name="logout"),
    path("signup/", accountsignup, name="signup"),
]

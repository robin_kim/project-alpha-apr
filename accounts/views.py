from django.shortcuts import render, redirect
from accounts.forms import AccountLoginForm, AccountSignUpForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User


def accountsignup(request):
    if request.method == "POST":
        form = AccountSignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = AccountSignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)


def accountlogin(request):
    if request.method == "POST":
        form = AccountLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = AccountLoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def accountlogout(request):
    logout(request)
    return redirect("login")

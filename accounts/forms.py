from django import forms


class AccountLoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput())


class AccountSignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput())
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput()
    )

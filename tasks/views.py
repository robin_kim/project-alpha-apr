from django.shortcuts import render, redirect
from tasks.forms import CreateTaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def my_task_list(request):
    my_task_list = Task.objects.filter(assignee=request.user)
    context = {
        "my_task_list": my_task_list,
    }
    return render(request, "tasks/show_my_tasks.html", context)
